require('dotenv').config();

module.exports = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,

  migrations: ['dist/migrations/**/*.js'],
  cli: {
    entitiesDir: 'src/**/*.{ts,js}',
    migrationsDir: 'src/migrations',
  },
  entities: ['dist/**/*.{entity,view}.{ts,js}'],
};
