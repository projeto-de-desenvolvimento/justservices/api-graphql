import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

@InputType()
export class CreateUserInput {
  @Field()
  @IsString()
  @IsNotEmpty({ message: 'Nome é obrigatório' })
  name: string;

  @Field()
  @IsString()
  @IsNotEmpty({ message: 'CPF é obrigatório' })
  cpf: string;

  @Field()
  @IsEmail()
  @IsNotEmpty({ message: 'E-mail é obrigatório' })
  email: string;

  @Field()
  @IsNotEmpty({ message: 'Senha é obrigatório' })
  password: string;
}
